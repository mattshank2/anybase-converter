//
//  ABCAppDelegate.h
//  AnyBase Converter
//
//  Created by Matt Shank on 6/15/13.
//  Copyright (c) 2013 Matt Shank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
