//
//  ABCViewController.h
//  AnyBase Converter
//
//  Created by Matt Shank on 6/15/13.
//  Copyright (c) 2013 Matt Shank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABCViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *convertedResult;
@property (weak, nonatomic) IBOutlet UITextField *numberToConvert;
@property (strong, nonatomic) IBOutlet UIPickerView *customWheel;

@end