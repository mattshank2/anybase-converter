//
//  ABCViewController.m
//  AnyBase Converter
//
//  Created by Matt Shank on 6/15/13.
//  Copyright (c) 2013 Matt Shank. All rights reserved.
//

#import "ABCViewController.h"
#import "Brain.h"

@interface ABCViewController ()

@property (nonatomic, strong) Brain* brain;
@property (nonatomic) NSInteger baseChoice;

@end

@implementation ABCViewController

@synthesize baseChoice = _baseChoice;
@synthesize brain = _brain;
@synthesize convertedResult = _convertedResult;
@synthesize numberToConvert = _numberToConvert;
@synthesize customWheel = _customWheel;

- (Brain*)brain
{
    if (!_brain) _brain = [[Brain alloc] init];
    return _brain;
}

- (UIPickerView*)customWheel
{
    if (!_customWheel) _customWheel = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 0.0, 170.0, 120.0)];
    
    return _customWheel;
}

- (IBAction)convertButtonPressed
{
    //  [self.convertedResult setText:[self.brain convert:[self.numberToConvert.text intValue] toBase:2];
//    self.convertedResult.text = [self.brain convert:[self.numberToConvert.text intValue] toBase:2];
    
    int selectedBase = 0;
    
    [self.numberToConvert resignFirstResponder];
    
    switch (self.baseChoice) {
        case 0:
            selectedBase = 2;
            break;
        case 1:
            selectedBase = 8;
            break;
        case 2:
            selectedBase = 16;
            break;
        case 3:
            break;
    }
    
    self.convertedResult.text = [self reverseString:[self.brain convert:[self.numberToConvert.text intValue] toBase:selectedBase]];

}

- (IBAction)baseHasBeenChanged:(UISegmentedControl *)sender
{
    self.baseChoice = sender.selectedSegmentIndex;
    [self.numberToConvert resignFirstResponder];
    if (![self.convertedResult.text isEqualToString:@""])
        self.convertedResult.text = @"";
    if (sender.selectedSegmentIndex == 3)
        self.customWheel.hidden = NO;
    else
        self.customWheel.hidden = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.numberToConvert resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    /*  limit to only numeric characters  */
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            return YES;
        }
    }
    
    /*  limit the users input to only 9 characters  */
    NSUInteger newLength = [self.numberToConvert.text length] + [string length] - range.length;
    return (newLength > 9) ? NO : YES;
}

-(NSString *)reverseString:(NSString*)myString
{
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [myString length];
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[myString substringWithRange:subStrRange]];
    }
    return reversedString;
}

@end
