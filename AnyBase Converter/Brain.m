//
//  Brain.m
//  AnyBase Converter
//
//  Created by Matt Shank on 6/15/13.
//  Copyright (c) 2013 Matt Shank. All rights reserved.
//

#import "ABCViewController.h"
#import "Brain.h"

@implementation Brain

- (NSString*)convert:(int)number toBase:(int)base
{
    NSMutableString* converted = [[NSMutableString alloc] init];
    int tempNumber = number;
    NSString* digit;
    int digitBeforeConvertingToString;
    
    switch (base) {
        case 2:
            do {
                digit = [NSString stringWithFormat:@"%d", tempNumber % base];
                [converted appendString: digit];
                tempNumber /= base;
            } while (tempNumber != 0);
            break;
        case 8:
            do {
                digit = [NSString stringWithFormat:@"%d", tempNumber % base];
                [converted appendString: digit];
                tempNumber /= base;
            } while (tempNumber != 0);
            break;
        case 16:
            do {
                digitBeforeConvertingToString = tempNumber % base;
                if (digitBeforeConvertingToString < 10)
                    digit = [NSString stringWithFormat:@"%d", digitBeforeConvertingToString];
                else
                    digit = [NSString stringWithFormat:@"%c", (char)digitBeforeConvertingToString + 'A' - 10];
                [converted appendString: digit];
                tempNumber /= base;
            } while (tempNumber != 0);
            break;
        default:
            break;
    }
    
    return converted;
}

@end
