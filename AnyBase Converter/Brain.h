//
//  Brain.h
//  AnyBase Converter
//
//  Created by Matt Shank on 6/15/13.
//  Copyright (c) 2013 Matt Shank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Brain : NSObject

- (NSString*)convert:(int)number toBase:(int)base;

@end
